cmake_minimum_required(VERSION 2.8)

#we assume third party dir is set

set( SDL_DIR ${THIRD_PARTY_DIR}/sdl/SDL2-2.0.7 )

set( SDL_INCLUDE_DIR ${THIRD_PARTY_DIR}/sdl/include )

file( GLOB SDL_INCLUDE_HEADERS ${SDL_INCLUDE_DIR}/SDL2/*.h )
source_group( sdl FILES ${SDL_INCLUDE_HEADERS} )
include_directories( ${SDL_INCLUDE_DIR} )

find_file( SDL2_DLL_RELEASE REQUIRED names SDL2.dll PATHS ${SDL_DIR}/VisualC/Win32/Release )
find_file( SDL2_DLL_DEBUG REQUIRED names SDL2d.dll PATHS ${SDL_DIR}/VisualC/Win32/Debug )
find_file( SDL2_LIB_RELEASE REQUIRED names SDL2.lib PATHS ${SDL_DIR}/VisualC/Win32/Release )
find_file( SDL2_LIB_DEBUG REQUIRED names SDL2d.lib PATHS ${SDL_DIR}/VisualC/Win32/Debug )
find_file( SDL2_MAIN_LIB_RELEASE REQUIRED names SDL2main.lib PATHS ${SDL_DIR}/VisualC/Win32/Release )
find_file( SDL2_MAIN_LIB_DEBUG REQUIRED names SDL2maind.lib PATHS ${SDL_DIR}/VisualC/Win32/Debug )

install( FILES ${SDL2_DLL_RELEASE} DESTINATION bin)
install( FILES ${SDL2_DLL_DEBUG} DESTINATION bin)

function( link_sdl targetname )
	target_link_libraries( ${targetname} optimized ${SDL2_LIB_RELEASE} debug ${SDL2_LIB_DEBUG} )
	target_link_libraries( ${targetname} optimized ${SDL2_MAIN_LIB_RELEASE} debug ${SDL2_MAIN_LIB_DEBUG} )
endfunction( link_sdl )